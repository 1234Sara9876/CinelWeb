﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CinelWeb.Models;

namespace CinelWeb.Controllers
{
    public class HomeController : Controller
    {
        DCinscricoesDataContext dc = new DCinscricoesDataContext();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Contato novoContato)
        {
            if (ModelState.IsValid)
            {
                dc.Contatos.InsertOnSubmit(novoContato);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }

    
}
