﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CinelWeb.Models;

namespace CinelWeb.Controllers
{
    public class InscritosController : Controller
    {
        DCinscricoesDataContext dc = new DCinscricoesDataContext();

        // GET: Contatos
        public ActionResult Index()
        {
            return View(dc.Inscritos);
        }

        // POST: Contatos/Create
        [HttpPost]
        public ActionResult Inscricao(Inscrito novaInscricao)
        {
            if (ModelState.IsValid)
            {
                dc.Inscritos.InsertOnSubmit(novaInscricao);
            }
            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index","Home");
            }
            catch
            {
                return View();
            }
        }
    }
}
