// JavaScript Document
function Acc_Function(id){
	"use strict";
	var x = document.getElementById(id);
	
	if(x.className.indexOf("w3-show") == -1){
		x.className += " w3-show";
	}
	else{
		x.className = x.className.replace(" w3-show", "");
	}
}

function ConfirmarSenha(input) {

    var x = document.getElementById('erro')
    var y = document.getElementById('validar')

    if (input.value != document.getElementById('senha').value) {
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
            y.setAttribute("disabled", "disabled");
        }
    }
    else{
        x.className = x.className.replace(" w3-show", "");
        y.removeAttribute("disabled");
    }
}